# log4rs-sentry

Sentry Appender for log4rs

## Usage

Add the following to your `Cargo.toml`:

```toml
log4rs-sentry = "^1.0"
```

Now, when configuring your `log4rs` logger, use the following code:

```rust
use log4rs_sentry::SentryAppender;

let sentry_encoder = PatternEncoder::new("{m}");
let sentry = SentryAppender::new(Box::new(sentry_encoder));

let config = Config::builder()
		.appender(Appender::builder().build("sentry", Box::new(sentry)))
		// other appenders
		.build(Root::builder()
				.appender("sentry")
				// other appenders
				.build(LevelFilter::Warn))
		.unwrap();
```
